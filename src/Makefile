#############################
# user configurable section #
#############################
INCLUDE=-I. -Icore -Imain -Imesh -Iutil	# Includes 
MARCH=-m64									# Specification of the architecture
OPT=-O3										# Compile options
LIBS = 										# Libraries

###############
# compilation #
###############

CC=gcc
CXX=g++
WARN=-Wall
CXXFLAGS=$(OPT) $(MARCH) $(INCLUDE) $(WARN) $(DEFS)
CCFLAGS=$(CXXFLAGS)

LIB_CXXSRCS = $(wildcard core/*.cpp main/*.cpp mesh/*.cpp util/*.cpp)
LIBOBJS = $(addprefix objs/, $(subst /,_,$(LIB_CXXSRCS:.cpp=.o)))
HEADERS = $(wildcard */*.h)

#$(info $$LIB_CXXSRCS is [${LIB_CXXSRCS}])
#$(info $$LIB_OBJS is [${LIB_OBJS}])
#$(info $$HEADERS is [${HEADERS}])

# Default build command: first init directories, then qhull.

all: default

default: dirs qhull

qhull: bin/qhull

dirs:
	/bin/mkdir -p bin objs lib

$(LIBOBJS): $(HEADERS)

.PHONY: dirs 
.SECONDARY:

# Build the core
lib/libqhull.a: $(LIBOBJS)
	@echo "Building the core qhull library (libqhull.a)"
	@ar rcs $@ $(LIBOBJS)
	
objs/core_%.o: core/%.cpp
	@echo "Building object $@"
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	
objs/mesh_%.o: mesh/%.cpp
	@echo "Building object $@"
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	
objs/util_%.o: util/%.cpp
	@echo "Building object $@"
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	
objs/main_%.o: main/%.cpp
	@echo "Building object $@"
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	
objs/qhull.o: main/qhull.cpp
	@echo "Building object $@"
	@$(CXX) $(CXXFLAGS) -o $@ -c $<
	
bin/qhull: objs/main_qhull.o lib/libqhull.a
	@echo "Linking $@"
	@$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBS)
	
clean:
	rm -f objs/* bin/* libs/*