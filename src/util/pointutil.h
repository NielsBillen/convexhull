/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		POINTUTIL.H
 *
 * Utility class which provides general function performing operations
 * on points.
 *
 *  Created on: 5 december 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "definitions.h"

#ifndef POINTUTIL_H_
#define POINTUTIL_H_

namespace qhull {

class Point;

void RemoveDoubles(vector<Point>& points, real epsilon = EPSILON);

}

#endif /* POINTUTIL_H_ */
