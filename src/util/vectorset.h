/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 																			 *
 * 		VECTORSET.h															 *
 * 																			 *
 * Utility class which provides storage using a vector backed up by a map    *
 * for and log(n) lookups.												     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef VECTORSET_H_
#define VECTORSET_H_

#include "definitions.h"

namespace qhull {

// VectorSet Declaration
template<class T, class Allocator = std::allocator<T> >
class vectorset {
public:
	// type definitions
	typedef Allocator allocator_type;
	typedef typename Allocator::size_type size_type;
	typedef typename vector<T>::iterator iterator;
	typedef typename vector<T>::const_iterator const_iterator;
	typedef typename map<T, uint32_t>::iterator lookup_it;
	typedef typename map<T, uint32_t>::const_iterator lookup_cit;

	typedef pair<T, uint32_t> lookup_pair;

	vectorset() {
	}

	vectorset(iterator start, iterator end) {
		for (iterator it = start; it != end; ++it)
			push_back(*it);
	}

	vectorset(size_type n) {
		vec.reserve(n);
	}

	inline uint32_t push_back(const T& t) {
		lookup_it it = lookup.find(t);
		if (it == lookup.end()) {
			uint32_t index = vec.size();
			vec.push_back(t);
			lookup.insert(lookup_pair(t, index));
			return index;
		} else
			return it->second;
	}

	inline const_iterator begin() const {
		return vec.begin();
	}

	inline const_iterator end() const {
		return vec.end();
	}

	inline const_iterator erase(uint32_t index) {
		// retrieve the item to be erased
		const T& t = vec[index];

		// perform assertions
		lookup_it it = lookup.find(t);

		Verify(it != lookup.end(), "vectorset::erase(uint32_t)::item to erase "
				"at the given index is not present in the lookup table");
		Verify(index == it->second, "vectorset::erase(uint32_t)::mismatch "
				"between erased index in array and matching index found "
				"in the lookup table!\n");

		// perform erasure
		lookup.erase(it);
		const_iterator result = vec.erase(vec.begin() + index);

		// update the indices
		for (lookup_it it = lookup.begin(); it != lookup.end(); ++it) {
			Verify(it->second != index, "vectorset::erase(uint32_t)::erase "
					"from lookup table failed!");
			if (it->second > index)
				it->second = it->second - 1;
		}

		return result;
	}

	inline bool erase(const T& t) {
		// find the item to erase
		lookup_it it = lookup.find(t);

		// return when not present
		if (it == lookup.end())
			return false;
		uint32_t index = it->second;

		// assert that the correct item is removed
		Verify(t == vec[index],
				"vectorset::erase(const T&) mismatch between erasure from "
						"array and erasure from lookup!\n");

		// perform removal
		vec.erase(vec.begin() + index);
		lookup.erase(it);

		// update the indices
		for (lookup_it it = lookup.begin(); it != lookup.end(); ++it) {
			Verify(it->second != index, "vectorset::erase(const T&)::erase "
					"from lookup table failed!");
			if (it->second > index)
				it->second = it->second - 1;
		}

		return true;
	}

	T pop_front() {
		const T& result = at(0);
		erase(result);
		return result;
	}

	T pop_back() {
		const T& result = at(size() - 1);
		erase(result);
		return result;
	}

	inline bool contains(const T& t) const {
		return lookup.find(t) != lookup.end();
	}

	inline bool index_of(const T& t, uint32_t &index) const {
		lookup_cit it = lookup.find(t);
		if (it == lookup.end())
			return false;
		index = it->second;
		return true;
	}

	inline const T & at(uint32_t index) const {
		return vec[index];
	}

	inline T at(uint32_t index) {
		return vec[index];
	}

	inline T operator[](uint32_t index) {
		return vec[index];
	}

	inline const T & operator[](uint32_t index) const {
		return vec[index];
	}

	inline size_type size() const {
		return vec.size();
	}

	inline lookup_cit sorted_begin() const {
		return lookup.begin();
	}

	inline lookup_cit sorted_end() const {
		return lookup.end();
	}
private:
	vector<T> vec;
	map<T, uint32_t> lookup;
};

}
#endif /* VECTORSET_H_ */
