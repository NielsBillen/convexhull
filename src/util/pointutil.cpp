/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		POINTUTIL.CPP
 *
 * Utility class which provides general function performing operations
 * on points.
 *
 *  Created on: 5 december 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "pointutil.h"
#include "point.h"
#include "vector.h"
#include <algorithm>
#include <iterator>

using namespace qhull;

/**
 * @brief Removes all the points which occur two times in the given vector.
 * 		  This operation is executed in O(n*log(n)) time by sorting the input
 * 		  vector and iterating over the elements, removing elements which are
 * 		  equal to their predecessor.
 *
 * @param points
 * 			the vector of points from which the common points will be removed.
 * @param epsilon
 * 			the maximum distance where two points are considered equal.
 */
void RemoveDoubles(vector<Point>& points, real epsilon) {
	if (points.size() < 2)
		return;
	std::sort(points.begin(), points.end());

	Point& current = points[0];
	for (point_iterator it = points.begin() + 1; it != points.end(); ++it)
		if ((*it - current).length() <= epsilon)
			it = points.erase(it);
		else
			current = *it;
}

