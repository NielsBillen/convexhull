/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		POINT.CPP
 *
 * Class representing a point in the three dimensional space.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "point.h"
#include "vector.h"

using namespace qhull;

Point::Point() :
		x(0), y(0), z(0) {
}

Point::Point(real x, real y, real z) :
		x(x), y(y), z(z) {
}

Point::Point(const Point& p) :
		x(p.x), y(p.y), z(p.z) {
}

Point& Point::operator=(const Point& p) {
	x = p.x;
	y = p.y;
	z = p.z;
	return *this;
}

bool Point::operator==(const Point &p) const {
	return x == p.x && y == p.y && z == p.z;
}

bool Point::operator()(const Point *left, const Point *right) const {
	return (*left) < (*right);
}

bool Point::operator()(const Point &left, const Point &right) const {
	return left < right;
}

bool Point::operator<(const Point& p) const {
	if (x < p.x)
		return true;
	else if (x > p.x)
		return false;
	else if (y < p.y)
		return true;
	else if (y > p.y)
		return false;
	else if (z < p.z)
		return true;
	else
		return false;
}

Point Point::operator+(const Point& p) const {
	return Point(x + p.x, y + p.y, z + p.z);
}

Point Point::operator+=(const Point &p) {
	x += p.x;
	y += p.y;
	z += p.z;
	return *this;
}

Vector Point::operator-(const Point& p) const {
	return Vector(x - p.x, y - p.y, z - +p.z);
}

Point& Point::operator-=(const Point &p) {
	x -= p.x;
	y -= p.y;
	z -= p.z;
	return *this;
}

void Point::print(FILE* file) const {
	fprintf(file, "%.6f %.6f %.6f\n", x, y, z);
}
