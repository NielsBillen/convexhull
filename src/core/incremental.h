/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		INCREMENTAL.H
 *
 * Class representing a point in the three dimensional space.
 *
 *  Created on: 4 december 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef INCREMENTAL_H_
#define INCREMENTAL_H_

#include "definitions.h"
#include "indexedmesh.h"
#include "quickhull.h"

namespace qhull {

void ConvexHull_Incremental(const vector<Point> &points,
		IndexedMesh &mesh);
}

#endif /* INCREMENTAL_H_ */
