/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		DEFINITIONS.CPP
 *
 * Utility class which includes all commonly used files and defines the
 * constants used in the program.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "definitions.h"
#include <sstream>

using namespace qhull;

namespace qhull {

void Verify(bool assertion, const char* message) {
#ifdef DEBUG
	if (!assertion) {
		fprintf(stderr, "Assert::Failed::%s\n", message);
		std::ostringstream stream;
		stream << "Assert::Failed::" << message;
		throw stream.str().c_str();
	}
#endif
}

}

