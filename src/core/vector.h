/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		VECTOR.H
 *
 * Class representing a vector in the three dimensional space.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef VECTOR_H_
#define VECTOR_H_

#include "definitions.h"
#include "point.h"

namespace qhull {

// Forward Declaration
class Point;

// Vector Class Declaration
class Vector {
public:
	Vector();
	Vector(const Vector& v);
	Vector(real x, real y, real z);

	Vector& operator=(const Vector& v);
	inline bool operator==(const Vector &v) const;

	inline bool operator<(const Vector &v) const;

	Vector operator+(const Vector& v) const;
	Vector& operator+=(const Vector &v);

	Vector operator-(const Vector& v) const;
	Vector& operator-=(const Vector &v);

	real length() const;
	real length_squared() const;

	void print(FILE* file) const;

	real x, y, z;
};

// Iterator Type Definitions
typedef vector<Vector>::iterator vector_iterator;
typedef vector<Vector>::const_iterator vector_const_iterator;

// Global Functions
real Dot(const Vector &v1, const Point &o);
real Dot(const Vector &v1, const Vector &v2);
Vector Normalize(const Vector &v);
Vector Cross(const Vector& v1, const Vector& v2);

}
#endif /* VECTOR_H_ */
