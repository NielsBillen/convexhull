/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		QUICKHULL.CPP
 *
 * Class containing the core methods for computing the convex hull of a list
 * of points.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "quickhull.h"
#include "vector.h"
#include "indexedmesh.h"
#include "indexedface.h"

/*
 * #include "quickhull.h"
 #include "vector.h"
 #include "indexedmesh.h"
 #include "indexedface.h"
 */

using namespace qhull;

namespace qhull {

PointDistance::PointDistance(const Point& point, real distance) :
		point(point), distance(distance) {
}

bool PointDistance::operator<(const PointDistance& d) const {
	return distance < d.distance;
}

bool PointDistanceComp::operator()(const PointDistance &left,
		const PointDistance &right) const {
	return right < left;
}

Edge::Edge() :
		from(0), to(0), inverted(false) {
}

Edge::Edge(uint32_t e1, uint32_t e2) {
	if (e1 < e2) {
		from = e1;
		to = e2;
		inverted = false;
	} else {
		from = e2;
		to = e1;
		inverted = true;
	}
}

Edge::Edge(const Edge &edge) :
		from(edge.from), to(edge.to), inverted(edge.inverted) {
}

Edge& Edge::operator =(const Edge& edge) {
	from = edge.from;
	to = edge.to;
	inverted = edge.inverted;

	return *this;
}

bool Edge::operator <(const Edge& edge) const {
	if (from < edge.from)
		return true;
	else if (from > edge.from)
		return false;
	else
		return to < edge.to;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		COMPUTE_CONVEX_HULL
 *
 * The method which computes the convex hull as described in the paper:
 * "The Quickhull Algorithm for Convex Hulls"
 *
 * @param points
 * 		the points to compute a tetrahedron with.
 * @param mesh
 * 		the mesh in which the tetrehedron will be stored.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void ConvexHull_Quick(const vector<Point> &input, IndexedMesh &mesh) {
	// make a copy of the input points.
	vector<Point> todo(input.begin(), input.end());

	// create a simplex of d + 1 points
	fprintf(stderr, "creating a simplex of d+1 points...\n");
	ComputeTetrahedron(todo, mesh);

	// for each face F
	//   for each unassigned point p
	//     if p is above F
	//	     assign p to F's outside set
	fprintf(stderr, "computing the outside sets...\n");
	outsidemap outside_map;
	ComputeOutsideSets(todo, mesh, outside_map);

	// for each face F with a non empty set
	fprintf(stderr, "entering the main loop\n");
	while (!outside_map.empty()) {
		const outside_map_pair& pair = *(outside_map.begin());
		const IndexedFace &face = pair.first;
		const outsideset &outside_set = pair.second;

		if (outside_set.empty()) {
			outside_map.erase(outside_map.begin());
			continue;
		}

		// select the furthest point p of F's outside set
		const Point &p = outside_set.begin()->point;

		// for all unvisited neighours N of facets in V
		//   if p is above N
		//     add N to V
		vector<IndexedFace> V;
		ComputeVisibleSet(face, p, V);

		fprintf(stderr, "computed the visible set\n");
		Verify(V.size() > 0, "ConvexHull_Quick(const vector<Point>&,"
				"IndexedMesh&)::the visible set should always be "
				"greater than one!\n");

		// the boundary of V is the set of horizon ridges H
		vector<vertex_pair> H;
		ComputeBoundary(V, H);

		fprintf(stderr, "computed the boundary set\n");
		Verify(H.size() > 0, "ConvexHull_Quick(const vector<Point>&,"
				"IndexedMesh&)::the boundary should always be "
				"greater than one!\n");

		for (face_const_iterator it = V.begin(); it != V.end(); ++it)
			Verify(mesh.erase(*it), "ConvexHull_Quick(const vector<Point>&,"
					"IndexedMesh&)::failed to erase one of the visible face\n");
		fprintf(stderr, "erased the visible faces\n");

		// for every ridge R in H
		vector<IndexedFace> N;
		for (edge_const_iterator it = H.begin(); it != H.end(); ++it) {
			// create a new facet for R and p
			// link the new facet to it's neighbours
			uint32_t new_index = mesh.push_back(it->first, it->second, p);
			N.push_back(mesh.get_face(new_index));
		}
		fprintf(stderr, "added the new faces\n");

		// determine all the points visible from the visible facets
		set<Point> visible_points;
		for (face_const_iterator it1 = V.begin(); it1 != V.end(); ++it1) {
			outsidemap::const_iterator it2 = outside_map.find(*it1);

			if (it2 == outside_map.end())
				continue;

			const outsideset& out = it2->second;
			for (outsideset::iterator it3 = out.begin(); it3 != out.end();
					++it3)
				if (!(it3->point == p))
					visible_points.insert(it3->point);
		}
		fprintf(stderr, "computed all the visible points\n");

		// for every new facet F'
		real distance;
		for (face_const_iterator it1 = N.begin(); it1 != N.end(); ++it1) {
			for (set<Point>::iterator it2 = visible_points.begin();
					it2 != visible_points.end(); ++it2) {
				if ((distance = it1->distance(*it2)) >= -EPSILON) {
					outsidemap_iterator it3 = outside_map.find(*it1);
					if (it3 == outside_map.end()) {
						outsideset outsideset;
						outsideset.insert(PointDistance(*it2, distance));
						outside_map.insert(outside_map_pair(*it1, outsideset));
					} else
						it3->second.insert(PointDistance(*it2, distance));
				}
			}
		}

		fprintf(stderr, "updated the new distances\n");

		for (face_const_iterator it1 = V.begin(); it1 != V.end(); ++it1) {
			outsidemap::iterator it2 = outside_map.find(*it1);
			if (it2 != outside_map.end())
				outside_map.erase(it2);
		}

		fprintf(stderr, "erasing old distances\n");
	}

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		COMPUTE_TETRAHEDRON
 *
 * Utility method which constructs a convex tetrahedron from the given vector.
 * The four points which are used to construct the tetrahedron are removed from
 * the given vector of points. The convex tetrahedron is stored inside the
 * given indexed mesh structure.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void ComputeTetrahedron(vector<Point> &points, IndexedMesh &mesh) {
	if (points.size() < 4) {
		fprintf(stderr,
				"ComputeTetrahedron::at least 4 points are needed to construct a "
						"tetrahedron! Only %lu are given!\n", points.size());
		return;
	}

	Point tetrahedron[4];

	// Find points which define the edges of the bounding box.
	uint32_t minx = 0, miny = 0, minz = 0;
	uint32_t maxx = 0, maxy = 0, maxz = 0;
	for (uint32_t i = 1; i < points.size(); ++i) {
		if (points[i].x < points[minx].x)
			minx = i;
		if (points[i].x > points[maxx].x)
			maxx = i;
		if (points[i].y < points[miny].y)
			miny = i;
		if (points[i].y > points[maxy].y)
			maxy = i;
		if (points[i].z < points[minz].z)
			minz = i;
		if (points[i].z > points[maxz].z)
			maxz = i;
	}

	// Compute the extend of the bounding box along the axis.
	real xsize = points[maxx].x - points[minx].x;
	real ysize = points[maxy].y - points[miny].y;
	real zsize = points[maxz].z - points[minz].z;

	// Use the two points with the largest extend as the starting point for the
	// tetrahedron.
	if (xsize > ysize && xsize > zsize) {
		tetrahedron[0] = points[minx];
		tetrahedron[1] = points[maxx];

		points.erase(points.begin() + max(minx, maxx));
		points.erase(points.begin() + min(minx, maxx));
	} else if (ysize > zsize) {
		tetrahedron[0] = points[miny];
		tetrahedron[1] = points[maxy];

		points.erase(points.begin() + max(miny, maxy));
		points.erase(points.begin() + min(miny, maxy));
	} else {
		tetrahedron[0] = points[minz];
		tetrahedron[1] = points[maxz];

		points.erase(points.begin() + max(minz, maxz));
		points.erase(points.begin() + min(minz, maxz));
	}

	// Find a third point which maximizes the ground plane of the tetrahedron.
	real max_area = 0;
	uint32_t best_index = 0;
	for (uint32_t i = 0; i < points.size(); ++i) {
		real area = Cross(tetrahedron[0] - points[i],
				tetrahedron[1] - points[i]).length_squared();
		if (area > max_area) {
			best_index = i;
			max_area = area;
			tetrahedron[2] = points[i];
		}
	}
	points.erase(points.begin() + best_index);

//	// Initialize the tetrahedron with the final three points in the vector.
//	for (uint32_t i = 0; i < 3; ++i) {
//		tetrahedron[i] = points.back();
//		points.pop_back();
//	}

	const Vector &v1 = tetrahedron[1] - tetrahedron[0];
	const Vector &v2 = tetrahedron[2] - tetrahedron[0];
	const Vector &n = Normalize(Cross(v1, v2));

	// Find the point which is furthest from the ground plane of the tetrahedron.
	uint32_t furthest_index = 0;
	tetrahedron[3] = points[0];
	real max_distance = Distance(tetrahedron[0], n, tetrahedron[3]);
	for (uint32_t i = 1; i < points.size(); ++i) {
		real distance = Distance(tetrahedron[0], n, points[i]);
		if (abs(distance) > abs(max_distance)) {
			furthest_index = i;
			tetrahedron[3] = points[i];
			max_distance = distance;
		}
	}

	// Erase the point from the vector.
	points.erase(points.begin() + furthest_index);

	// Store the tetrahedron in the mesh with correctly oriented normals.
	if (max_distance >= EPSILON) {
		uint32_t f1 = mesh.push_back(tetrahedron[0], tetrahedron[2],
				tetrahedron[1]);
		uint32_t f2 = mesh.push_back(tetrahedron[2], tetrahedron[0],
				tetrahedron[3]);
		uint32_t f3 = mesh.push_back(tetrahedron[1], tetrahedron[2],
				tetrahedron[3]);
		uint32_t f4 = mesh.push_back(tetrahedron[0], tetrahedron[1],
				tetrahedron[3]);

		Verify(mesh.get_face(f1).distance(tetrahedron[3]) <= -EPSILON,
				"incorrect orientation for f1 when max_distance>EPSILON");
		Verify(mesh.get_face(f2).distance(tetrahedron[1]) <= -EPSILON,
				"incorrect orientation for f2 when max_distance>EPSILON");
		Verify(mesh.get_face(f3).distance(tetrahedron[0]) <= -EPSILON,
				"incorrect orientation for f3 when max_distance>EPSILON");
		Verify(mesh.get_face(f4).distance(tetrahedron[2]) <= -EPSILON,
				"incorrect orientation for f4 when max_distance>EPSILON");
	} else if (max_distance <= -EPSILON) {
		uint32_t f1 = mesh.push_back(tetrahedron[0], tetrahedron[1],
				tetrahedron[2]);
		uint32_t f2 = mesh.push_back(tetrahedron[0], tetrahedron[2],
				tetrahedron[3]);
		uint32_t f3 = mesh.push_back(tetrahedron[2], tetrahedron[1],
				tetrahedron[3]);
		uint32_t f4 = mesh.push_back(tetrahedron[1], tetrahedron[0],
				tetrahedron[3]);

		Verify(mesh.get_face(f1).distance(tetrahedron[3]) <= -EPSILON,
				"incorrect orientation for f1 when max_distance <= -EPSILON");
		Verify(mesh.get_face(f2).distance(tetrahedron[1]) <= -EPSILON,
				"incorrect orientation for f2 when max_distance <= -EPSILON");
		Verify(mesh.get_face(f3).distance(tetrahedron[0]) <= -EPSILON,
				"incorrect orientation for f3 when max_distance <= -EPSILON");
		Verify(mesh.get_face(f4).distance(tetrahedron[2]) <= -EPSILON,
				"incorrect orientation for f4 when max_distance <= -EPSILON");
	} else
		fprintf(stderr, "ComputeTetrahedron::error ran out of precision!\n");
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		REMOVE_INTERNAL_POINTS
 *
 * Utility method which removes all the points from the vector which are on
 * the inside of the given mesh.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void RemoveInternalPoints(vector<Point> &points, IndexedMesh &mesh) {
	for (point_iterator it = points.begin(); it != points.end(); ++it) {
		bool inside = true;
		for (face_const_iterator fit = mesh.face_begin();
				fit != mesh.face_end(); ++fit) {
			if (fit->distance(*it) >= -EPSILON) {
				inside = false;
				break;
			}
		}
		if (inside)
			it = points.erase(it);
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		COMPUTE_OUTSIDE_SETS
 *
 * Utility which determines for the visible points for every face of the mesh.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void ComputeOutsideSets(const vector<Point> &points, const IndexedMesh& mesh,
		outsidemap &outside_map) {

	// Iterate over all the faces.
	for (face_const_iterator it = mesh.face_begin(); it != mesh.face_end();
			++it) {
		real distance;
		outsideset outside_set;

		// Iterate over all the points
		for (point_const_iterator pit = points.begin(); pit != points.end();
				++pit) {
			if ((distance = it->distance(*pit)) >= -EPSILON)
				outside_set.insert(PointDistance(*pit, distance));
		}

		if (outside_set.size() > 0)
			outside_map.insert(outside_map_pair(*it, outside_set));
	}
}

void ComputeVisibleSet(const IndexedMesh &mesh, const Point &point,
		vector<IndexedFace>& visible) {
	for (face_const_iterator it = mesh.face_begin(); it != mesh.face_end();
			++it) {
		if (it->distance(point) >= -EPSILON)
			visible.push_back(*it);
	}
}

void ComputeVisibleSet(const IndexedFace& face, const Point &point,
		vector<IndexedFace>& visible) {
	IndexedMesh* mesh = face.get_mesh();

	set<IndexedFace> visited;
	vector<IndexedFace> todo;
	todo.push_back(face);

	while (!todo.empty()) {
		const IndexedFace& f = todo.back();
		todo.pop_back();

		if (visited.find(f) != visited.end())
			break;

		visited.insert(f);

		if (f.distance(point) >= -EPSILON) {
			visible.push_back(f);
			mesh->get_neighbours(f, todo);
		}
	}
}

void ComputeBoundary(const vector<IndexedFace> &faces,
		vector<vertex_pair>& boundary) {
	map<Edge, uint32_t> boundary_map;

	for (face_const_iterator fit = faces.begin(); fit != faces.end(); ++fit) {
		const IndexedFace& face = *fit;

		for (uint32_t i = 0; i < 3; ++i) {
			uint32_t e1 = face.get_index(i);
			uint32_t e2 = face.get_index((i + 1) % 3);

			Edge edge(e1, e2);

			map<Edge, uint32_t>::iterator edge_it = boundary_map.find(edge);
			if (edge_it == boundary_map.end())
				boundary_map.insert(pair<Edge, uint32_t>(edge, 1u));
			else
				edge_it->second = edge_it->second + 1;
		}
	}

	for (map<Edge, uint32_t>::const_iterator it = boundary_map.begin();
			it != boundary_map.end(); ++it) {
		if (it->second == 1) {
			if (it->first.inverted)
				boundary.push_back(vertex_pair(it->first.to, it->first.from));
			else
				boundary.push_back(vertex_pair(it->first.from, it->first.to));

		}
	}
}

}
