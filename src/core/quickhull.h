/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		QUICKHULL.H
 *
 * Class containing the core methods for computing the convex hull of a list
 * of points.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef QHULL_CORE_QUICKHULL_H_
#define QHULL_CORE_QUICKHULL_H_

#include "definitions.h"
#include "point.h"
#include "indexedmesh.h"

namespace qhull {

// Forward Declarations
class Point;
class IndexedMesh;
class IndexedFace;

// Point Distance Declaration
class PointDistance {
public:
	PointDistance(const Point& point, real distance);
	bool operator<(const PointDistance& d) const;

	Point point;
	real distance;
};

// PointDistanceComp Declaration
class PointDistanceComp {
public:
	bool operator()(const PointDistance &left,
			const PointDistance &right) const;
};

// Edge Declaration
class Edge {
public:
	Edge();
	Edge(const Edge &edge);
	Edge(uint32_t e1, uint32_t e2);
	Edge& operator=(const Edge&);
	bool operator<(const Edge& edge) const;

	uint32_t from;
	uint32_t to;
	bool inverted;
};

typedef set<PointDistance, PointDistanceComp> outsideset;
typedef outsideset::iterator outsideset_iterator;
typedef outsideset::const_iterator outsideset_const_iterator;
typedef map<IndexedFace, outsideset> outsidemap;
typedef outsidemap::iterator outsidemap_iterator;
typedef outsidemap::const_iterator outsidemap_const_iterator;
typedef pair<IndexedFace, outsideset> outside_map_pair;

typedef vector<vertex_pair>::iterator edge_iterator;
typedef vector<vertex_pair>::const_iterator edge_const_iterator;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		COMPUTE_CONVEX_HULL
 *
 * The method which computes the convex hull as described in the paper:
 * "The Quickhull Algorithm for Convex Hulls"
 *
 * @param points
 * 		the points to compute a tetrahedron with.
 * @param mesh
 * 		the mesh in which the tetrehedron will be stored.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void ConvexHull_Quick(const vector<Point> &points, IndexedMesh &mesh);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		COMPUTE_TETRAHEDRON
 *
 * Utility method which constructs a convex tetrahedron from the given vector.
 * The four points which are used to construct the tetrahedron are removed from
 * the given vector of points. The convex tetrahedron is stored inside the
 * given indexed mesh structure.
 *
 * @param points
 * 		the points to compute a tetrahedron with.
 * @param mesh
 * 		the mesh in which the tetrehedron will be stored.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void ComputeTetrahedron(vector<Point> &points, IndexedMesh &mesh);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		REMOVE_INTERNAL_POINTS
 *
 * Utility method which removes all the points from the vector which are on
 * the inside of the given mesh.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void RemoveInternalPoints(vector<Point> &points, IndexedMesh &mesh);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		COMPUTE_OUTSIDE_SETS
 *
 * Utility which determines for the visible points for every face of the mesh.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void ComputeOutsideSets(const vector<Point> &points, const IndexedMesh& mesh,
		outsidemap &outside);

void ComputeVisibleSet(const IndexedMesh& mesh, const Point &point,
		vector<IndexedFace>& visible);

void ComputeVisibleSet(const IndexedFace& face, const Point &point,
		vector<IndexedFace>& visible);

void ComputeBoundary(const vector<IndexedFace> &faces, vector<vertex_pair>& boundary);
}

#endif
