/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		TRIANGLE.CPP
 *
 * Class representing a triangle in the three dimensional space.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "triangle.h"

using namespace qhull;

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) {
	points[0] = p1;
	points[1] = p2;
	points[2] = p3;
	n = Normalize(Cross(p2 - p1, p3 - p1));
}

Triangle::Triangle(const Triangle &triangle) {
	memcpy(points, triangle.points, 3 * sizeof(Point));
	n = triangle.n;
}
Point Triangle::operator[](uint32_t index) const {
	return points[index];
}

Vector Triangle::normal() const {
	return n;
}

void Triangle::print(FILE *file) const {
	fprintf(file,
			"[Triangle]:\n\t%.6f %.6f %.6f\n\t%.6f %.6f %.6f\n\t%.6f %.6f %.6f\n\tNormal: %.6f %.6f %.6f\n",
			points[0].x, points[0].y, points[0].z, points[1].x, points[1].y,
			points[1].z, points[2].x, points[2].y, points[2].z, n.x, n.y, n.z);
}

namespace qhull {

real Distance(const Point &o, const Vector &n, const Point &p) {
//	real offset = -Dot(n, o);

	return Dot(n, p - o);
}

real Distance(const Point& p1, const Point& p2, const Point &p3,
		const Point &point) {
	const Vector &normal = Normalize(Cross(p2 - p1, p3 - p1));
	return Distance(p1, normal, point);
}

real Area(const Point& p1, const Point& p2, const Point &p3) {
	return Cross(p2 - p1, p3 - p1).length();
}
}
