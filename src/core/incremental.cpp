/*
 * incremental.cpp
 *
 *  Created on: 4 dec. 2014
 *      Author: niels
 */

#include "incremental.h"

namespace qhull {

void ConvexHull_Incremental(const vector<Point> &points, IndexedMesh &mesh) {
	vector<Point> todo(points.begin(), points.end());
	for (uint32_t i = 0; i < points.size(); ++i) {
		const Point &p1 = points[i];
		bool add = true;
		for (uint32_t j = i + 1; j < points.size(); ++j) {
			const Point &p2 = points[j];

			if (p1 == p2 || (p1 - p2).length() <= EPSILON) {
				fprintf(stdout,
						"ConvexHull_Incremental::removed identical point!\n");
				add = false;
				break;
			}
		}
		if (add)
			todo.push_back(p1);
	}

	ComputeTetrahedron(todo, mesh);

	for (point_const_iterator pit = todo.begin(); pit != todo.end(); ++pit) {
		vector<IndexedFace> visible;
		ComputeVisibleSet(mesh, *pit, visible);
		if (visible.size() == 0)
			continue;

		vector<vertex_pair> boundary;
		ComputeBoundary(visible, boundary);

		for (vector<IndexedFace>::const_iterator it = visible.begin();
				it != visible.end(); ++it)
			mesh.erase(*it);

		for (vector<vertex_pair>::const_iterator it = boundary.begin();
				it != boundary.end(); ++it)
			mesh.push_back(it->first, it->second, *pit);
	}
}

}
