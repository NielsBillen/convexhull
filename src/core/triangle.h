/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		TRIANGLE.H
 *
 * Class representing a triangle in the three dimensional space.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "definitions.h"
#include "point.h"
#include "vector.h"

namespace qhull {

// Triangle Class Declaration
class Triangle {
public:
	Triangle(const Point &p1, const Point &p2, const Point &p3);
	Triangle(const Triangle &triangle);
	Point operator[](uint32_t index) const;
	Vector normal() const;
	void print(FILE *file) const;
private:
	Point points[3];
	Vector n;
};

typedef vector<Triangle>::iterator triangle_it;
typedef vector<Triangle>::iterator triangle_cit;

real Distance(const Point &o, const Vector &n, const Point &p);
real Distance(const Point& p1, const Point& p2, const Point &p3,
		const Point &point);
real Area(const Point& p1, const Point& p2, const Point &p3);
}
#endif /* TRIANGLE_H_ */
