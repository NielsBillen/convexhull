/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		POINT.H
 *
 * Class representing a point in the three dimensional space.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef QHULL_CORE_POINT_H_
#define QHULL_CORE_POINT_H_

#include "definitions.h"

namespace qhull {

// Forward Declaration
class Vector;

// Point Class Declaration
class Point {
public:
	Point();
	Point(real x, real y, real z);
	Point(const Point& p);

	Point& operator=(const Point& p);
	bool operator==(const Point &p) const;

	bool operator()(const Point *left, const Point *right) const;
	bool operator()(const Point &left, const Point &right) const;

	bool operator<(const Point& p) const;

	Point operator+(const Point& p) const;
	Point operator+=(const Point &p);

	Vector operator-(const Point& p) const;
	Point& operator-=(const Point &p);

	void print(FILE* file) const;

	real x, y, z;
};

// Iterator type definitions
typedef vector<Point>::iterator point_iterator;
typedef vector<Point>::const_iterator point_const_iterator;

}

#endif
