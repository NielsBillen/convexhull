/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		VECTOR.CPP
 *
 * Class representing a vector in the three dimensional space.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "vector.h"

using namespace qhull;

Vector::Vector() :
		x(0), y(0), z(0) {
}

Vector::Vector(const Vector& v) :
		x(v.x), y(v.y), z(v.z) {
}

Vector::Vector(real x, real y, real z) :
		x(x), y(y), z(z) {
}

Vector& Vector::operator=(const Vector& v) {
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}

inline bool Vector::operator==(const Vector &v) const {
	return x == v.x && y == v.y && z == v.z;
}

inline bool Vector::operator<(const Vector &v) const {
	if (x < v.x)
		return true;
	else if (x > v.x)
		return false;
	else if (y < v.y)
		return true;
	else if (y > v.y)
		return false;
	else if (z < v.z)
		return true;
	else
		return false;
}

Vector Vector::operator+(const Vector& v) const {
	return Vector(x + v.x, y + v.y, z + v.z);
}

Vector& Vector::operator+=(const Vector &v) {
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

Vector Vector::operator-(const Vector& v) const {
	return Vector(x - v.x, y - v.y, z - +v.z);
}

Vector& Vector::operator-=(const Vector &v) {
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

real Vector::length() const {
	return sqrt(x * x + y * y + z * z);
}

real Vector::length_squared() const {
	return x * x + y * y + z * z;
}

void Vector::print(FILE* file) const {
	fprintf(file, "%.6f %.6f %.6f\n", x, y, z);
}

namespace qhull {
	real Dot(const Vector &v1, const Vector &v2) {
		return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	}

	real Dot(const Vector &v1, const Point &o) {
		return v1.x * o.x + v1.y * o.y + v1.z * o.z;
	}

	Vector Normalize(const Vector &v) {
		real inv_length = 1.f / v.length();
		return Vector(v.x * inv_length, v.y * inv_length, v.z * inv_length);
	}

	Vector Cross(const Vector& v1, const Vector& v2) {
		real v1x = v1.x, v1y = v1.y, v1z = v1.z;
		real v2x = v2.x, v2y = v2.y, v2z = v2.z;
		return Vector((v1y * v2z) - (v1z * v2y), (v1z * v2x) - (v1x * v2z),
				(v1x * v2y) - (v1y * v2x));
	}
}
