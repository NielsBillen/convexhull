/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		DEFINITIONS.H
 *
 * Utility class which includes all commonly used files and defines the
 * constants used in the program.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef QHULL_CORE_DEFINITIONS_H_
#define QHULL_CORE_DEFINITIONS_H_

#ifndef DEBUG
#define DEBUG
#endif

// include common files.
#include <stdio.h>
#include <stdint.h>
#include <cstdlib>
#include <vector>
#include <map>
#include <set>
#include <string.h>
#include <math.h>

// use the common containers
using std::vector;
using std::set;
using std::map;
using std::pair;
using std::min;
using std::max;

namespace qhull {

// definition to quickly change between single/double precision
#ifdef PRECISION_SINGLE

typedef float real;
static const real EPSILON =1e-6f;

#else

typedef double real;
static const real EPSILON = 1e-6;

#endif

void Verify(bool assertion, const char* message);

}

#endif
