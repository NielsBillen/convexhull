/*
 * indexedface.cpp
 *
 *  Created on: 2 dec. 2014
 *      Author: niels
 */

#include "indexedface.h"
#include "point.h"
#include "vector.h"

using namespace qhull;

IndexedFace::IndexedFace() {
	mesh = NULL;
	for (uint32_t i = 0; i < 3; ++i)
		indices[i] = i;
}

IndexedFace::IndexedFace(const IndexedFace &face) :
		mesh(face.mesh) {
	for (uint32_t i = 0; i < 3; ++i)
		indices[i] = face.indices[i];
}

IndexedFace::IndexedFace(IndexedMesh* mesh, uint32_t i1, uint32_t i2,
		uint32_t i3) :
		mesh(mesh) {
	if (i1 < i2 && i1 < i3) {
		indices[0] = i1;
		indices[1] = i2;
		indices[2] = i3;
	} else if (i2 < i3) {
		indices[0] = i2;
		indices[1] = i3;
		indices[2] = i1;
	} else {
		indices[0] = i3;
		indices[1] = i1;
		indices[2] = i2;
	}
}

IndexedFace& IndexedFace::operator =(const IndexedFace &face) {
	mesh = face.mesh;
	for (uint32_t i = 0; i < 3; ++i)
		indices[i] = face.indices[i];
	return *this;
}

void IndexedFace::triangulate(vector<Triangle> &triangles) const {
	const Point& p1 = mesh->get_point(indices[0]);
	const Point& p2 = mesh->get_point(indices[1]);
	const Point& p3 = mesh->get_point(indices[2]);
	triangles.push_back(Triangle(p1, p2, p3));
}

void IndexedFace::print(FILE *file) const {
	fprintf(file, "[IndexedFace]: %u %u %u\n", indices[0], indices[1],
			indices[2]);
//	vector<Triangle> triangles;
//	triangulate(triangles);
//	for (triangle_cit it = triangles.begin(); it != triangles.end(); ++it)
//		it->print(file);
}

Vector IndexedFace::normal() const {
	const Point& p1 = mesh->get_point(indices[0]);
	const Point& p2 = mesh->get_point(indices[1]);
	const Point& p3 = mesh->get_point(indices[2]);
	return Normalize(Cross(p2 - p1, p3 - p1));
}

float IndexedFace::distance(const Point &point) const {
	const Point& p1 = mesh->get_point(indices[0]);
	const Point& p2 = mesh->get_point(indices[1]);
	const Point& p3 = mesh->get_point(indices[2]);
	return Distance(p1, p2, p3, point);
}

bool IndexedFace::operator<(const IndexedFace &face) const {
	if (indices[0] < face.indices[0])
		return true;
	else if (indices[0] > face.indices[0])
		return false;
	else if (indices[1] < face.indices[1])
		return true;
	else if (indices[1] > face.indices[1])
		return false;
	else
		return indices[2] < face.indices[2];
}

bool IndexedFace::operator()(const IndexedFace *left,
		const IndexedFace* right) const {
	return (*left) < (*right);
}

bool IndexedFace::operator()(const IndexedFace &left,
		const IndexedFace &right) const {
	return left < right;
}

bool IndexedFace::operator==(const IndexedFace &face) const {
	return mesh == face.mesh && indices[0] == face.indices[0]
			&& indices[1] == face.indices[1] && indices[2] == face.indices[2];
}

uint32_t IndexedFace::get_index(uint32_t index) const {
	return indices[index];
}

IndexedMesh* IndexedFace::get_mesh() const {
	return mesh;
}
