/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		INDEXEDFACE.H
 *
 * Class representing a triangle face in the three dimensional space, backed by
 * an indexed triangle mesh.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef INDEXEDFACE_H_
#define INDEXEDFACE_H_

#include "definitions.h"
#include "indexedmesh.h"

namespace qhull {

// Forward Declarations
class Point;
class Triangle;
class IndexedMesh;
class IndexedFace;

// IndexedFaceDeclaration
class IndexedFace {
public:
	friend class IndexedMesh;

	IndexedFace(const IndexedFace &face);

	IndexedFace& operator=(const IndexedFace &face);
	bool operator==(const IndexedFace &face) const;

	bool operator()(const IndexedFace *left, const IndexedFace* right) const;
	bool operator()(const IndexedFace &left, const IndexedFace &right) const;
	bool operator<(const IndexedFace &face) const;

	Vector normal() const;
	float distance(const Point &point) const;

	uint32_t get_index(uint32_t index) const;
	IndexedMesh* get_mesh() const;

	void triangulate(vector<Triangle> &triangles) const;

	void print(FILE *file) const;
protected:
	IndexedFace();
	IndexedFace(IndexedMesh* mesh, uint32_t i1, uint32_t i2, uint32_t i3);

	IndexedMesh* mesh;
	uint32_t indices[3];
};

// Iterator type definitions
typedef pair<pair<uint32_t, uint32_t>, IndexedFace> face_pair;
typedef vectorset<IndexedFace>::iterator face_iterator;
typedef vectorset<IndexedFace>::const_iterator face_const_iterator;

}

#endif /* INDEXEDFACE_H_ */
