/*
 * indexedmesh.cpp
 *
 *  Created on: 2 dec. 2014
 *      Author: niels
 */

#include "indexedmesh.h"

using namespace qhull;

IndexedMesh::IndexedMesh() {
}

uint32_t IndexedMesh::push_back(const Point &p1, const Point &p2,
		const Point &p3) {
	// insert the points and retrieve their indices from the array.
	uint32_t i1 = points.push_back(p1);
	uint32_t i2 = points.push_back(p2);
	uint32_t i3 = points.push_back(p3);

	return push_back(i1, i2, i3);
}

uint32_t IndexedMesh::push_back(uint32_t i1, uint32_t i2, const Point &p3) {
	uint32_t i3 = points.push_back(p3);
	return push_back(i1, i2, i3);
}

uint32_t IndexedMesh::push_back(uint32_t i1, uint32_t i2, uint32_t i3) {
	// initialize the face.
	IndexedFace face(this, i1, i2, i3);

	uint32_t index;
	if (faces.index_of(face, index)) {
		// the face is already present in the mesh

		// verify the presence of it's vertices
		Verify(vertexmap.find(vertex_pair(i1, i2)) != vertexmap.end(),
				"IndexedMesh::push_back::one of the vertices is missing "
						"in the mesh!\n");
		Verify(vertexmap.find(vertex_pair(i2, i3)) != vertexmap.end(),
				"IndexedMesh::push_back::one of the vertices is missing "
						"in the mesh!\n");
		Verify(vertexmap.find(vertex_pair(i3, i1)) != vertexmap.end(),
				"IndexedMesh::push_back::one of the vertices is missing "
						"in the mesh!\n");
		return index;
	} else {
		index = faces.push_back(face);

		Verify(vertexmap.find(vertex_pair(i1, i2)) == vertexmap.end(),
				"IndexedMesh::push_back::one of the vertices is already present"
						" in the map!\n");
		Verify(vertexmap.find(vertex_pair(i2, i3)) == vertexmap.end(),
				"IndexedMesh::push_back::one of the vertices is already present"
						" in the map!\n");
		Verify(vertexmap.find(vertex_pair(i3, i1)) == vertexmap.end(),
				"IndexedMesh::push_back::one of the vertices is already present"
						" in the map!\n");

		vertexmap.insert(face_pair(vertex_pair(i1, i2), face));
		vertexmap.insert(face_pair(vertex_pair(i2, i3), face));
		vertexmap.insert(face_pair(vertex_pair(i3, i1), face));

		return index;
	}
}

const Point& IndexedMesh::get_point(uint32_t index) const {
	Verify(index >= 0 && index < points.size(),
			"IndexedMesh::get_point()::index is out of bounds!");
	return points[index];
}

const IndexedFace& IndexedMesh::get_face(uint32_t index) const {
	Verify(index >= 0 && index < faces.size(),
			"IndexedMesh::get_point()::index is out of bounds!");
	return faces[index];
}

face_const_iterator IndexedMesh::face_begin() const {
	return faces.begin();
}

face_const_iterator IndexedMesh::face_end() const {
	return faces.end();
}

vectorset<IndexedFace>::size_type IndexedMesh::num_faces() const {
	return faces.size();
}

point_const_iterator IndexedMesh::point_begin() const {
	return points.begin();
}

point_const_iterator IndexedMesh::point_end() const {
	return points.end();
}

vectorset<Point>::size_type IndexedMesh::num_points() const {
	return points.size();
}

bool IndexedMesh::contains(const IndexedFace &face) const {
	return faces.contains(face);
}

bool IndexedMesh::erase(const IndexedFace &face) {
	uint32_t index;
	if (faces.index_of(face, index)) {
		// the face is present in the mesh

		// erase all the edges
		for (uint32_t i = 0; i < 3; ++i) {
			uint32_t from = face.get_index(i);
			uint32_t to = face.get_index((i + 1) % 3);

			vertex_pair edge(from, to);
			vertex_map_iterator it = vertexmap.find(edge);

			Verify(it != vertexmap.end(),
					"IndexedMesh::erase(const IndexedFace&)::one of the "
							"vertices of the face is not present in the mesh!\n");
			Verify(it->second == face,
					"IndexedMesh::erase(const IndexedFace&)::the face "
							"corresponding to one of the erased edges does not match!");

			vertexmap.erase(it);
		}

		faces.erase(index);
		return true;
	} else
		return false;
}

void IndexedMesh::get_neighbours(const IndexedFace &face,
		vector<IndexedFace> &neighbours) const {
	if (!faces.contains(face))
		return;

	pair<uint32_t, uint32_t> p1(face.get_index(1), face.get_index(0));
	pair<uint32_t, uint32_t> p2(face.get_index(2), face.get_index(1));
	pair<uint32_t, uint32_t> p3(face.get_index(0), face.get_index(2));

	map<pair<uint32_t, uint32_t>, IndexedFace>::const_iterator it;
	if ((it = vertexmap.find(p1)) != vertexmap.end())
		neighbours.push_back(it->second);
	if ((it = vertexmap.find(p2)) != vertexmap.end())
		neighbours.push_back(it->second);
	if ((it = vertexmap.find(p3)) != vertexmap.end())
		neighbours.push_back(it->second);
}

void IndexedMesh::print(FILE* file) const {
	fprintf(file, "[IndexedMesh]\n");
	for (map<IndexedFace, uint32_t>::const_iterator it = faces.sorted_begin();
			it != faces.sorted_end(); ++it) {
		fprintf(file, "\t");
		it->first.print(file);
	}
}

void IndexedMesh::to_obj(FILE* file) const {
	for (point_const_iterator it = points.begin(); it != points.end(); ++it)
		fprintf(file, "v  %g %g %g\n", it->x, it->y, it->z);
	fprintf(file, "\n");
	for (face_const_iterator it = faces.begin(); it != faces.end(); ++it)
		fprintf(file, "f  %u %u %u\n", it->get_index(0) + 1,
				it->get_index(1) + 1, it->get_index(2) + 1);

}
