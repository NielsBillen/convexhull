/*
 * indexedmesh.h
 *
 *  Created on: 2 dec. 2014
 *      Author: niels
 */

#ifndef INDEXEDMESH_H_
#define INDEXEDMESH_H_

#include "definitions.h"
#include "triangle.h"
#include "vector.h"
#include "vectorset.h"
#include "indexedface.h"

namespace qhull {

// Forward Declarations
class Point;
class Vector;
class IndexedFace;
class IndexedMesh;

// Type definitions
typedef pair<uint32_t, uint32_t> vertex_pair;
typedef map<vertex_pair, IndexedFace>::iterator vertex_map_iterator;

// Vertex pair comparator
class VertexPairComp {
public:
	bool operator()(const vertex_pair &left, const vertex_pair &right) const {
		if (left.first < right.first)
			return true;
		else if (left.first > right.first)
			return false;
		else
			return left.second < right.second;
	}
};

// IndexedMesh Declaration
class IndexedMesh {
public:
	IndexedMesh();

	uint32_t push_back(const Point &p1, const Point &p2, const Point &p3);
	uint32_t push_back(uint32_t i1, uint32_t i2, const Point &p3);
	uint32_t push_back(uint32_t i1, uint32_t i2, uint32_t i3);

	const Point& get_point(uint32_t index) const;
	const IndexedFace& get_face(uint32_t index) const;

	vector<IndexedFace>::const_iterator face_begin() const;
	vector<IndexedFace>::const_iterator face_end() const;
	vectorset<IndexedFace>::size_type num_faces() const;

	vector<Point>::const_iterator point_begin() const;
	vector<Point>::const_iterator point_end() const;
	vectorset<Point>::size_type num_points() const;

	bool erase(const IndexedFace &face);

	bool contains(const IndexedFace &face) const;

	void get_neighbours(const IndexedFace &face,
			vector<IndexedFace> &neighbours) const;

	void print(FILE* file) const;

	void to_obj(FILE* file) const;
private:
	map<vertex_pair, IndexedFace, VertexPairComp> vertexmap;
	vectorset<IndexedFace> faces;
	vectorset<Point> points;
};

}

#endif /* INDEXEDMESH_H_ */
