/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 		QHULL.CPP
 *
 *	Entry point of the program.
 *
 *  Created on: 29 november 2014
 *  Author: Niels Billen
 *  Version: 0.1
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "indexedmesh.h"
#include "point.h"
#include "triangle.h"
#include "incremental.h"
#include "quickhull.h"

using namespace qhull;

void print_usage();
int main(int argc, char**argv);

int main(int argc, char **argv) {
	if (argc == 0) {
		print_usage();
		return 0;
	}

	vector<Point> points;
	for (uint32_t i = 0; i < 100; ++i) {
		double x = (double) rand() / RAND_MAX;
		double y = (double) rand() / RAND_MAX;
		double z = (double) rand() / RAND_MAX;
		points.push_back(Point(x, y, z));

	}

	IndexedMesh mesh;
	ConvexHull_Incremental(points, mesh);

	fprintf(stdout, "convex hull has %lu faces\n", mesh.num_faces());

	for (point_const_iterator it = points.begin(); it != points.end(); ++it) {
		for (face_const_iterator fit = mesh.face_begin();
				fit != mesh.face_end(); ++fit) {
			real distance = fit->distance(*it);

			try {
				Verify(distance <= EPSILON, "convex hull algorithm failed!");
			} catch (const char* e) {
				fprintf(stderr, "%s:: distance is %.8f\n", e, distance);
				std::exit(1);
			}
		}
	}

	FILE* file;
	file = fopen("convexhull.obj", "w");
	mesh.to_obj(file);
	fclose(file);

}

void print_usage() {
	fprintf(stdout, "usage:\n");
}

